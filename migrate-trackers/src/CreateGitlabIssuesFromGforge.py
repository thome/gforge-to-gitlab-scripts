'''
Created on 25 avr. 2019

@author: dvojtise
Usage:
    grab the csv files from your gforge (a link is available on the top of the tracker)
    first, either change the global variables (on top of this file) or set the corresponding arguments on the command line
    some maps are used to help having better messages
    get a personalAccessToken (from https://gitlab.inria.fr/profile/personal_access_tokens)
    you can use a 'bot' account that is owner of the project (note if this is part of a group the bot account must also be owner of the group)
    CreateGitlabIssuesFromGforge.py -i <inputfile> -t <gitlabPersonalAccessToken> -l <comma_separated_url-safe_labels> -p <gitlab project name, including group>
    If this script doesn't suit your needs, do not hesitate to adapt it ;-)
Limitations:
    - does not copy the comments and  joigned artifacts (not available in csv export :-( )
    - cannot set the reporter (gitlab API limitation cf. https://gitlab.com/gitlab-org/gitlab-ce/issues/23144)
    => as a workaround, the description is prefixed with a message indicating the original tracker issue, the reporter, 
        additionaly, the script should be run using a "bot" account accessToken.
    => discussion : gitlab administrators may be able to achieve it using "sudo api" https://gitlab.com/help/api/README#sudo    

Dev notes:
    https://docs.gitlab.com/ee/api/issues.html#new-issue
    some tutos I used to learn how to write this script : 
        https://realpython.com/api-integration-in-python/
'''
import sys, getopt
import csv
import requests
import urllib.parse

gforgetrackerurl = 'https://gforge.inria.fr/tracker/?func=detail'
gitlaburl = 'https://gitlab.inria.fr/api/v4'
gitlabProjectName = 'mytestgroup/testproject'

priorityToLabel = {'1': '', '2': 'prio - low', '3': 'prio - medium', '4': 'prio - high', '5': 'prio - top'}

# list of user names that exists in gitlab, (used to get better header in the description)
# if a user is not known, it will fall back to plain text 
userMap = {'Didier Vojtisek': '@dvojtise', 'Guillaume M.': '@gmoynard'}

personalAccessToken = 'EzX-***REPLACE-ME****TAyQBsGmD'

mainLabels = "bug,imported%20from%gforge"  # comma separated list of labels (must be url encoded )

class APIError(Exception):
    """An API Error Exception"""

    def __init__(self, status):
        self.status = status

    def __str__(self):
        return "APIError: {}".format(self.status)

def main(argv):
    global gforgetrackerurl
    global personalAccessToken
    global mainLabels
    global gitlabProjectName
    inputfile = ''
    try:
        opts, args = getopt.getopt(argv,"hi:t:l:p:",["ifile=","accesstoken=","labels=","gitlabprojectname="])
    except getopt.GetoptError:
        print ('CreateGitlabIssuesFromGforge.py -i <inputfile> --accesstoken=<gitlabPersonalAccessToken> --labels=<comma_separated_url-safe_labels> --gitlabprojectname=<projectname>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print ('CreateGitlabIssuesFromGforge.py -i <inputfile> --accesstoken=<gitlabPersonalAccessToken> --labels=<comma_separated_url-safe_labels> --gitlabprojectname=<projectname>')
            sys.exit()
        elif opt in ("-i", "--ifile"):
            inputfile = arg
        elif opt in ("-t", "--accesstoken"):
            personalAccessToken = arg
        elif opt in ("-l", "--labels"):
            mainLabels = arg
        elif opt in ("-p", "--gitlabprojectname"):
            gitlabProjectName =  arg
    print("inputfile={}".format(inputfile))
    print("accesstoken={}".format(personalAccessToken))
    print("labels={}".format(mainLabels))
    print("gitlabProjectName={}".format(gitlabProjectName))
    processFromCSV(inputfile)

def processFromCSV( fileName):
    print("importing {}".format(fileName))
    with open(fileName, 'r') as csvFile:
        reader = csv.DictReader(csvFile)
        for row in reader:
            print(row)
            add_task(row['summary'], row['details'], row['submitter_name'], row['open_date'], row['last_modified_date'], row['priority'], row['status_name'], row['artifact_id'])
    csvFile.close()

def _url(path):
    return gitlaburl + path

def convertDate(gforgeDate):
    from datetime import datetime
    dateObject = datetime.strptime(gforgeDate,'%d/%m/%Y %H:%M')
    return dateObject.isoformat()

def add_task(summary, description, originalAuthor, creationDate, lastModifiedDate, priority, statusName, originIssueId):
    importMessage = ('__Imported issue:__ This issue was initially reported by _' + 
                     userMap.get(originalAuthor, originalAuthor) + 
                     '_ in ' +  gforgetrackerurl + '&aid=' + originIssueId +
                     ' (Additionnal comments haven\'t been imported and may still be there)\n---\n') 
    
    if priorityToLabel[priority] == '':
        labels = mainLabels
    else:
        labels = mainLabels + ',' + urllib.parse.quote(priorityToLabel[priority])
     
    headers = { "PRIVATE-TOKEN": personalAccessToken}
    url = _url("/projects/{}/issues?title={}&labels={}&description={}&created_at={}".
               format(urllib.parse.quote(gitlabProjectName, safe=''), 
                      urllib.parse.quote(summary, safe=''),
                      labels,
                      urllib.parse.quote((importMessage+description).replace('\n', '\n\n'), safe=''), 
                      convertDate(creationDate)
                      ))
    print('\t' + url)
    resp = requests.post(url, headers=headers)
    if resp.status_code != 201:
                print(resp)
                raise APIError('{} POST {}'.format(resp, url))
#    print(resp)
    createIssueId = str(resp.json()['iid'])
    print('\tissue #' + createIssueId + ' created')
        
# last modification date and closed issues        
# if closed, must close it using the modify issue API   
    if  statusName == 'Closed':
        url =  _url("/projects/{}/issues/{}?updated_at={}&state_event=close".
               format(urllib.parse.quote(gitlabProjectName, safe=''), 
                      createIssueId,
                      convertDate(lastModifiedDate)
                      ))
    else:
        url =  _url("/projects/{}/issues/{}?updated_at={}".
               format(urllib.parse.quote(gitlabProjectName, safe=''), 
                      createIssueId,
                      convertDate(lastModifiedDate)
                      ))
    print('\t' + url)
    resp = requests.put(url, headers=headers)
    if resp.status_code != 200:
                print(resp)
                raise APIError('{} PUT {}'.format(resp, url ))
    print('\tissue #' + createIssueId + ' modified')
    
if __name__ == '__main__':
    main(sys.argv[1:])