# gforge-to-gitlab-scripts

Some scripts to migrate content (trackers) from gforge to gitlab

## migrate-trackers

### Usage:

- grab the csv files from your gforge (a link is available on the top of the tracker)
- first, either change the global variables (on top of the python file) or set the corresponding arguments on the command line
- some maps are used to help having better messages
- get a personalAccessToken (from https://gitlab.inria.fr/profile/personal_access_tokens)
you can use a 'bot' account that is owner of the project (note if this is part of a group the bot account must also be owner of the group)
    CreateGitlabIssuesFromGforge.py -i <inputfile> -t <gitlabPersonalAccessToken> -l <comma_separated_url-safe_labels> -p <gitlab project name, including group>

- If this script doesn't suit your needs, do not hesitate to adapt it ;-)

### Limitations:

- does not copy the comments and  joigned artifacts (not available in csv export :-( )
- cannot set the reporter (gitlab API limitation cf. https://gitlab.com/gitlab-org/gitlab-ce/issues/23144)

=> as a workaround, the description is prefixed with a message indicating the original tracker issue, the reporter, 
        additionaly, the script should be run using a "bot" account accessToken.

=> discussion : gitlab administrators may be able to achieve it using "sudo api" https://gitlab.com/help/api/README#sudo    

### Dev notes:
- https://docs.gitlab.com/ee/api/issues.html#new-issue
